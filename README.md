## Database SSL Check

Your users connect to your site with an encrypted connection.
No one would ever think to run a site without an SSL cert!

But what about the backend? Are you encrypting the connection
to the database? (If you're not sure, the answer is probably no.)

This module adds entries to the system Status Report detailing
how your site connects to the database, the cipher used
(if any), and the client library version.

### Requirements

No special requirements at this time.

### Install

Install module via composer.

### Usage

Database SSL information or status will be displayed on Status
report page: _/admin/reports/status_
